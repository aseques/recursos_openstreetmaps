# README #

El procediment per fer la importació de les dades del població del cadastre a
openstreetmaps és una mica complicat, aquí he documentat alguns dels pasos més dubtosos

### Programes ###

* La guia espanyola del per fer la importació és [aquí](https://wiki.openstreetmap.org/wiki/ES:Catastro_espa%C3%B1ol/Importaci%C3%B3n_de_edificios/Gu%C3%ADa_de_importaci%C3%B3n/Gesti%C3%B3n_de_proyectos#Generar_y_corregir_los_archivos_a_importar)
* Es poden comprovar els errors des de [osmose](http://osmose.openstreetmap.fr/ca/map/#zoom=14&lat=42.1794&lon=2.50247&item=5xxx&level=1%2C2%2C3&tags=&fixable=)


### Programes a instal·lar ###

* Cal canviar l'idioma al català des del setup.py, d'aquesta manera ens generarà un csv amb equivalències entre el nom

### Tasques ###

Un cop feta la primera passada del catatom2osm tindrem generat un fitxer que es diu highway_names.csv, s'ha d revisar el
llistat de noms de carrers que hi ha:

* Si és només a l'esquesrra és un carrer sense equivalència a OSM (masia, disseminat, ..)
* Si només està a la columna dreta, és un carrer només existent a OSM
* El cas òptim és que els noms estiguin als dos costats.

Per fer aquesta revisio la forma més fàcil és obrir el csv en un excel (o google docs) i anar marcant a la tercera columna 
els valors que hagim revisat (i anar-los corregint a la segona columna). 
Un cop tinguem tot el fitxer revisat, esborrem la tercera columna i exportem el fitxer com a TSV (fitxer separat per tabulacions).
El fitxer s'ha de dir igualment highway_names.csv i l'hem de guardar a la mateix ubicació per seguir amb la importació.

Si esborrem el fitxer current_highway.osm i tornem a executar el catatosm xxxx ens generarà els fitxers necessaris pel cadastre

* Haurem de carregar els fitxers amb problemes al josm (veure review.txt)

Un cop hagim solucionat els problemes amb els fitxers problemàtics nomes caldrà fer un pull request a [github](https://github.com/OSM-es/catastro-import)